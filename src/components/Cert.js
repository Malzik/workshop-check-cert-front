import React, {useEffect, useState} from "react";
import moment from "moment";

export const Cert = ({cert}) => {
    const [dateFormatted, setDateFormatted] = useState();

    useEffect(() => {
        setDateFormatted(moment(cert.key_expire).format('DD-MM-YYYY'))
    }, [cert])

    const isActive = () => {
        const now = moment()
        const expired = moment(cert.key_expire)
        const diff = expired.diff(now)

        return diff > 0;
    }

    return (
        <tbody>
            <tr>
                <td>Id</td>
                <td>{cert.serial}</td>
            </tr>
            <tr>
                <td>Nom de domaine</td>
                <td>{cert.name}</td>
            </tr>
            <tr>
                <td>Algorithme de signature</td>
                <td>{cert.signature}</td>
            </tr>
            <tr>
                <td>Date d'expiration</td>
                <td>{dateFormatted}</td>
            </tr>
            <tr>
                <td>Statut</td>
                <td>{isActive() ? "Valid": "Invalid"}</td>
            </tr>
            <tr>
                <td>Taille</td>
                <td>{cert.key_length} bits</td>
            </tr>
        </tbody>
    )
}
