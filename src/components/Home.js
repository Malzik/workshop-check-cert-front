import React, {useState} from 'react';
import {FaSearch} from "react-icons/all";
import {certApi} from "../service/cert";
import {Cert} from "./Cert";
import './Home.css';

export const Home = () => {
    const [cert, setCert] = useState({})
    const [websiteUrl, setWebsiteUrl] = useState("")

    const handleChange = e => {
        setWebsiteUrl(e.target.value)
    }
    const search = () => {
        setCert({})
        certApi
            .getCert("check_cert/" + websiteUrl + "/")
            .then(result => setCert({...result, name: websiteUrl}))
            .catch(err => console.log(err))
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            search()
        }
    }

    return (
        <div className={"bg-gray-700 text-white h-screen"}>
            <div className={"container mx-auto shadow-2xl bg-gray-600 h-full"}>
                <h1 className={"text-center text-4xl pt-14"}>Recherchez le site de votre choix</h1>
                <div className="pt-14 flex w-10/12 mx-auto">
                    <FaSearch size={28}/>
                    <input className="w-full rounded p-2 text-black" type="text" placeholder="Lien du site" onChange={handleChange} onKeyDown={handleKeyDown} />
                    <button className="bg-red-light hover:bg-red-lighter rounded text-white p-2 pl-4 pr-4" onClick={search}>
                        <p className="font-semibold text-xl">Search</p>
                    </button>
                </div>
                <div className="pt-14">
                    <table>
                        {cert.name !== undefined ? <Cert cert={cert} /> : null}
                    </table>
                </div>
            </div>
        </div>
    )
}
