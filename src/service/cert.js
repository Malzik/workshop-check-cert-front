import { requestApi } from "./request";

export const certApi = {
    getHelloWorld: () =>
        new Promise((resolve, reject) => {
            requestApi
                .get("hello/")
                .then(res =>resolve(res))
                .catch(err => reject(err))
        }),
    getCert: websiteUrl =>
        new Promise((resolve, reject) => {
            requestApi
                .get(websiteUrl)
                .then(res =>resolve(res))
                .catch(err => reject(err))
        }),
}
